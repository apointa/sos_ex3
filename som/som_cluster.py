import random
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import CSS4_COLORS
from matplotlib.cm import get_cmap
from matplotlib.patches import Circle, Rectangle, Wedge

from sklearn.cluster import KMeans, AgglomerativeClustering
from collections import defaultdict, Counter


def generate_kmeans_map(map_weights, x_dim, y_dim, n_clusters, preprocess=None):
    """
    Generates a clustered som with kmeans clustering
    :param map_weights: SOM weights (each row is a unit, each column one dimension)
    :param x_dim: x dimension of som
    :param y_dim: y dimension of som
    :param n_clusters: number of clusters
    :param preprocess: Callable which preprocesses the map weights before clustering (return value is new map weights)
    :return: clustered map (2D - X and Y dim of som - value is the assigned cluster class)
    """
    cluster_data = __generate_kmeans_clustering(map_weights, n_clusters, preprocess)
    return __generate_cluster_map(cluster_data, x_dim, y_dim)


def generate_hierarchy_map(map_weights, x_dim, y_dim, n_clusters, linkage='ward', preprocess=None):
    """
    Generates a clustered som with hierarchy clustering
    :param map_weights: SOM weights (each row is a unit, each column one dimension)
    :param x_dim: x dimension of som
    :param y_dim: y dimension of som
    :param n_clusters: number of clusters
    :param linkage: linkage used for the hierarchy clustering ('single', 'complete', 'ward)
    :param preprocess: Callable which preprocesses the map weights before clustering (return value is new map weights)
    :return: clustered map (2D - X and Y dim of som - value is the assigned cluster class)
    """
    cluster_data = __generate_hierarchy_clustering(map_weights, n_clusters, linkage, preprocess)
    return __generate_cluster_map(cluster_data, x_dim, y_dim)


def generate_data_kmeans_map(unit_map, vec_data, n_clusters, preprocess=None):
    """
    Generates a cluster map, clustering the vector data with kmeans clustering
    :param unit_map: Map<Key, Val> - Key: coordinates of unit (x,y) - Val: list of assigned vecs (index val in vec_data df)
    :param vec_data: 2D (array or df) - rows are the vectors - columns the dimensions
    :param n_clusters: number of clusters
    :param preprocess: Callable which preprocesses the vec_data before clustering (return value is new vec_data)
    :return: Map<Key1,Map<Key2, int>> Key1: coordinates of unit (x,y) - Key2 (int): cluster class - int: count of the class
    """
    cluster_data = __generate_kmeans_clustering(vec_data, n_clusters, preprocess)
    return __generate_data_cluster_map(unit_map, cluster_data)


def generate_data_hierarchy_map(unit_map, vec_data, n_clusters, linkage='ward', preprocess=None):
    """
    Generates a cluster map, clustering the vector data with hierarchy clustering
    :param unit_map: Map<Key, Val> - Key: coordinates of unit (x,y) - Val: list of assigned vecs (index val in vec_data df)
    :param vec_data: 2D (array or df) - rows are the vectors - columns the dimensions
    :param n_clusters: number of clusters
    :param linkage: linkage used for the hierarchy clustering ('single', 'complete', 'ward)
    :param preprocess: Callable which preprocesses the vec_data before clustering (return value is new vec_data)
    :return: Map<Key1,Map<Key2, int>> Key1: coordinates of unit (x,y) - Key2 (int): cluster class - int: count of the class
    """
    cluster_data = __generate_hierarchy_clustering(vec_data, n_clusters, linkage, preprocess)
    return __generate_data_cluster_map(unit_map, cluster_data)


def plot_cluster_map(cluster_map, unit_map=None, data_cluster_map=None, color_list=None, circle_size_ratio=0.1,
                     unit_size=1.0):
    """
    Plot SOM Clustered - Optionally also the assigned data vector can be shown unit with frequency indicator and optionally also clustered
    :param cluster_map: clustered map (2D - X and Y dim of som - value is the assigned cluster class)
    :param unit_map: If set the assigned data vector are shown with unit frequency indicator
    :param data_cluster_map: If set the assigned data vector are shown with unit frequency indicator and also clustered
    :param color_list: custom list of colors used for the cluster classes (min size is number of clusters)
    :param circle_size_ratio: Ration between the smallest and largest data frequency circles
    :param unit_size: size of one unit cell
    """
    cluster_arr = cluster_map.to_numpy()
    clusters = set(np.unique(cluster_arr))
    x_dim = cluster_arr.shape[1]
    y_dim = cluster_arr.shape[0]

    # Get Colors
    colors = color_list if color_list else __get_color_list(len(clusters))

    if data_cluster_map:
        data_func = __get_data_cluster_plot_func(data_cluster_map, 'major', circle_size_ratio, unit_size,
                                                 color_list=colors)
    elif unit_map:
        data_func = __get_unit_map_plot_func(unit_map, circle_size_ratio, unit_size)
    else:
        data_func = None, y_dim, x_dim

    def set_axis(axis, x_pos, y_pos):
        axis.add_patch(Rectangle((unit_size*x_pos, unit_size*y_pos), unit_size*1, unit_size*1,
                                 color=colors[cluster_arr[y_pos, x_pos]], alpha=0.9, zorder=0))
        if callable(data_func):
            data_func(axis, x_pos, y_pos)

    __plot_map(y_dim, x_dim, set_axis, unit_size)


def plot_data_cluster_map(data_cluster_map, x_dim, y_dim, mode='major', color_list=None, circle_size_ratio=0.1,
                          unit_size=1.0):
    """
    Plot SOM with Data Clustered
    :param data_cluster_map: Map<Key1,Map<Key2, int>> Key1: coordinates of unit (x,y) - Key2 (int): cluster class - int: count of the class
    :param x_dim: x dimension of som
    :param y_dim: y dimension of som
    :param mode: major: major cluster class of unit is shown with color, pie: pie chart with different cluster classes
    :param color_list: custom list of colors used for the cluster classes (min size is number of clusters)
    :param circle_size_ratio: Ration between the smallest and largest data frequency circles
    :param unit_size: size of one unit cell
    """
    set_axis = __get_data_cluster_plot_func(data_cluster_map, mode, circle_size_ratio, unit_size, color_list=color_list)
    __plot_map(y_dim, x_dim, set_axis, unit_size)


def __get_data_cluster_plot_func(data_cluster_map, mode, circe_size_ratio, unit_size, color_list=None):
    # Generate Help Data
    clusters = set()
    max_per_unit = 0
    min_per_unit = sys.maxsize
    max_major_per_unit = 0
    min_major_per_unit = sys.maxsize
    for (x, y), counter in data_cluster_map.items():
        if len(counter) > 0:
            clusters.update(counter.keys())
            max_per_unit = max(max_per_unit, sum(counter.values()))
            max_major_per_unit = max(max_major_per_unit, max(counter.values()))
            min_per_unit = min(min_per_unit, sum(counter.values()))
            min_major_per_unit = min(min_major_per_unit, max(counter.values()))

    # Get Colors
    colors = color_list if color_list else __get_color_list(len(clusters))

    if mode == 'major':
        get_size = __get_size_func(min_major_per_unit, max_major_per_unit, circe_size_ratio, max_size=0.5*unit_size)

        def set_axis(axis, x_pos, y_pos):
            major = data_cluster_map[(x_pos, y_pos)].most_common(1)
            if len(major) > 0:
                axis.add_patch(Circle((unit_size*(x_pos + 0.5), unit_size*(y_pos + 0.5)), get_size(major[0][1]),
                                      color=colors[major[0][0]]))

        return set_axis

    elif mode == 'pie':
        get_size = __get_size_func(min_per_unit, max_per_unit, circe_size_ratio, max_size=0.5*unit_size)

        def set_axis(axis, x_pos, y_pos):
            class_counts = data_cluster_map[(x_pos, y_pos)].items()
            col = [colors[counts[0]] for counts in class_counts]
            cnt = [counts[1] for counts in class_counts]
            for w in __get_pie(cnt, col, (unit_size*(x_pos + 0.5), unit_size*(y_pos + 0.5)), get_size(sum(cnt))):
                axis.add_patch(w)

        return set_axis
    else:
        raise Exception("Mode not supported!")


def __get_unit_map_plot_func(unit_map, circe_size_ratio, unit_size):
    max_per_unit = 0
    min_per_unit = sys.maxsize
    for _, vecs in unit_map.items():
        if len(vecs) > 0:
            max_per_unit = max(max_per_unit, len(vecs))
            min_per_unit = min(min_per_unit, len(vecs))
    get_size = __get_size_func(min_per_unit, max_per_unit, circe_size_ratio, max_size=0.5*unit_size)

    def set_axis(axis, x_pos, y_pos):
        vecs = unit_map[(x_pos, y_pos)]
        if len(vecs) > 0:
            axis.add_patch(Circle((unit_size * (x_pos + 0.5), unit_size * (y_pos + 0.5)), get_size(len(vecs)),
                                  color='dimgrey'))

    return set_axis


def __get_size_func(min_count, max_count, small_big_ration, max_size):
    min_circe_size = max_size * small_big_ration
    return lambda count: (min_circe_size + (
            (count - min_count) / (max_count - min_count) * (
                max_size - min_circe_size))) if min_count != max_count else max_size


def __get_color_list(count):
    if count <= 10:
        return get_cmap("tab10").colors[:count]
    elif count <= 20:
        return get_cmap("tab20").colors[:count]
    else:
        return random.sample(list(CSS4_COLORS), count)


def __get_pie(counts, colors, coords, size):
    all_counts = sum(counts)
    wedges = []
    current_theta = 0
    for i, cnt in enumerate(counts):
        next_theta = min(360, current_theta + cnt / all_counts * 360)
        wedges.append(Wedge(coords, size, theta1=current_theta, theta2=next_theta, color=colors[i]))
        current_theta = next_theta
    return wedges


def __plot_map(y_dim, x_dim, set_on_axis, unit_size):
    fig, ax = plt.subplots(figsize=(unit_size*x_dim, unit_size*y_dim))
    ax.set_xlim((0, unit_size*x_dim))
    ax.set_ylim((unit_size*y_dim, 0))
    major_ticks_x = np.arange(0, unit_size*x_dim, unit_size)
    major_ticks_y = np.arange(0, unit_size*y_dim, unit_size)
    ax.set_xticks(major_ticks_x)
    ax.set_yticks(major_ticks_y)
    ax.grid(which='both', color='black')
    ax.tick_params(labelsize=0)

    for y in range(y_dim):
        for x in range(x_dim):
            set_on_axis(ax, x, y)
    plt.show()


def __generate_cluster_map(cluster_data, x_dim, y_dim):
    cluster_df = pd.DataFrame(cluster_data.values.reshape(y_dim, x_dim))
    return cluster_df


def __generate_data_cluster_map(unit_map, data_cluster_data):
    cluster_map = defaultdict(Counter)
    for coords, vec_ids in unit_map.items():
        cluster_list = list()
        for iD in vec_ids:
            cluster_list.append(data_cluster_data[iD])
        cluster_map[coords] = Counter(cluster_list)
    return cluster_map


def __generate_kmeans_clustering(vec_data, n_clusters, preprocess):
    prep_data = preprocess(vec_data) if preprocess else vec_data
    kmeans = KMeans(n_clusters=n_clusters)
    prediction = kmeans.fit_predict(prep_data)
    return pd.Series(prediction, index=pd.Index(vec_data.index))


def __generate_hierarchy_clustering(vec_data, n_cluster, linkage, preprocess):
    prep_data = preprocess(vec_data) if preprocess else vec_data
    agglo = AgglomerativeClustering(n_clusters=n_cluster, linkage=linkage)
    prediction = agglo.fit_predict(prep_data)
    return pd.Series(prediction, index=pd.Index(vec_data.index))
