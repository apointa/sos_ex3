import pandas as pd
import numpy as np
import gzip
from collections import defaultdict


class SOMMap:
    """
    Represents a SOMMap with all properties necessary for Visualizations
    """

    def __init__(self, map_weight_df, vec_dim, x_dim, y_dim):
        """
        :param map_weight_df: SOM weights (each row is a unit, each column one dimension)
        :param vec_dim: data dimension of SOM
        :param x_dim: x_dim of SOM
        :param y_dim: y_dim of SOM
        """
        self.map_weight_df = map_weight_df
        self.vec_dim = vec_dim
        self.x_dim = x_dim
        self.y_dim = y_dim

    @classmethod
    def from_somtoolbox(cls, trained_map_file):
        """
        :param trained_map_file: *.wgt file generated from the som toolbox
        :return: SOMMap
        """
        map_weight_df, vec_dim, x_dim, y_dim = SOMToolBox_Parse(trained_map_file).read_weight_file()
        return cls(map_weight_df, vec_dim, x_dim, y_dim)

    @classmethod
    def from_minisom(cls, som):
        """
        :param som: SOM trained with MiniSom
        :return: SOMMap
        """
        x_dim = som._weights.shape[0]
        y_dim = som._weights.shape[1]
        vec_dim = som._weights.shape[2]
        reshaped = som._weights.transpose((1, 0, 2)).reshape(-1, vec_dim)
        map_weight_df = pd.DataFrame(reshaped)
        return cls(map_weight_df, vec_dim, x_dim, y_dim)


class UnitsMapWithLinkedVectors:
    """
    Holds/Generates the information which input vectors are at which unit
    """

    def __init__(self, unit_map, input_vec_df):
        """
        :param unit_map: Map<Key, Val> - Key: coordinates of unit (x,y) - Val: list of assigned vecs (index val in vec_data df)
        :param input_vec_df: vector dataframe - rows are the vectors - columns the dimensions
        """
        self.unit_map = unit_map
        self.input_vec_df = input_vec_df

    @classmethod
    def from_somtoolbox(cls, trained_units_file, input_vec_file):
        """
        :param trained_units_file: *.unit file generated from the som toolbox
        :param input_vec_file: *.vec file which was used in the som toolbox
        :return: UnitsMapWithLinkedVectors
        """
        unit_map = UnitsMapWithLinkedVectors.__read_unit_map_assoziation(trained_units_file)
        input_vec_df = read_somtoolbox_input_to_df(input_vec_file)
        return cls(unit_map, input_vec_df)

    @classmethod
    def from_minisom(cls, som, input_vecs):
        """
        :param som: SOM trained with MiniSom
        :param input_vecs: data vectors which should be linked
        :return: UnitsMapWithLinkedVectors
        """
        #som_map = SOMMap.from_minisom(som)
        #weight_arr = som_map.map_weight_df.to_numpy()
        input_vec_df = pd.DataFrame(input_vecs)
        unit_dict = defaultdict(list)
        for i, vec in enumerate(input_vecs):
            #position = np.argmin(np.sqrt(np.sum(np.power(weight_arr - vec, 2), axis=1)))
            #x, y = position % som_map.x_dim, position // som_map.x_dim
            unit_dict[som.winner(vec)].append(i)
        return cls(unit_dict, input_vec_df)

    @staticmethod
    def __read_unit_map_assoziation(trained_map_units):
        """
        Check which input vectors are assigned to which map units - from som toolbox unit file
        """
        unit_dict = defaultdict(list)
        current_list = list()
        y_dim = None
        with open(trained_map_units, 'r') as file:
            for line in file.readlines():
                if line.startswith('$YDIM'):
                    y_dim = int(line.split(' ')[1].strip())
                elif line.startswith('$POS_X'):
                    x_pos = int(line.split(' ')[1].strip())
                elif line.startswith('$POS_Y'):
                    y_pos = int(line.split(' ')[1].strip())
                    current_list = unit_dict[(x_pos, y_pos)]
                elif not line.startswith("$"):
                    current_list.append(line.strip())
        return unit_dict


def read_somtoolbox_input_to_df(input_vec_file):
    """
    Read the input vectors - from som toolbox input vector file to a dataframe
    """
    with open(input_vec_file, 'r') as file:
        for line in file.readlines():
            if line.startswith('$VEC_DIM'):
                vec_dim = int(line.split(' ')[1].strip())
                vec_df = pd.DataFrame(columns=[int(x) for x in range(vec_dim)])
            elif not line.startswith("$"):
                line_dat = line.strip().split(' ')
                row = pd.Series(line_dat[:-1], dtype=float)
                row_df = pd.DataFrame([row], index=[line_dat[-1]])
                vec_df = pd.concat([vec_df, row_df], axis=0)
    return vec_df


class SOMToolBox_Parse:
    """
    This class is from the template notebook provided by course instructor
    It is used to parse a somtoolbox wgt file
    """

    def __init__(self, filename):
        self.filename = filename

    def read_weight_file(self, ):
        df = pd.DataFrame()
        if self.filename[-3:len(self.filename)] == '.gz':
            with gzip.open(self.filename, 'rb') as file:
                df, vec_dim, xdim, ydim = self._read_vector_file_to_df(df, file)
        else:
            with open(self.filename, 'rb') as file:
                df, vec_dim, xdim, ydim = self._read_vector_file_to_df(df, file)

        file.close()
        return df.astype('float64'), vec_dim, xdim, ydim

    def _read_vector_file_to_df(self, df, file):
        xdim, ydim, vec_dim, position = 0, 0, 0, 0
        for byte in file:
            line = byte.decode('UTF-8')
            if line.startswith('$'):
                xdim, ydim, vec_dim = self._parse_vector_file_metadata(line, xdim, ydim, vec_dim)
                if xdim > 0 and ydim > 0 and len(df.columns) == 0:
                    df = pd.DataFrame(index=range(0, ydim * xdim), columns=range(0, vec_dim))
            else:
                if len(df.columns) == 0 or vec_dim == 0:
                    raise ValueError('Weight file has no correct Dimensional information.')
                position = self._parse_weight_file_data(line, position, vec_dim, df)
        return df, vec_dim, xdim, ydim

    def _parse_weight_file_data(self, line, position, vec_dim, df):
        splitted = line.split(' ')
        try:
            df.values[position] = list(np.array(splitted[0:vec_dim]).astype(float))
            position += 1
        except:
            raise ValueError('The input-vector file does not match its unit-dimension.')
        return position

    def _parse_vector_file_metadata(self, line, xdim, ydim, vec_dim):
        splitted = line.split(' ')
        if splitted[0] == '$XDIM':
            xdim = int(splitted[1])
        elif splitted[0] == '$YDIM':
            ydim = int(splitted[1])
        elif splitted[0] == '$VEC_DIM':
            vec_dim = int(splitted[1])
        return xdim, ydim, vec_dim
