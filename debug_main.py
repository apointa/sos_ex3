import minisom as som
from som.som_data import UnitsMapWithLinkedVectors, SOMMap, read_somtoolbox_input_to_df
from som.som_cluster import plot_data_cluster_map, generate_data_kmeans_map, generate_data_hierarchy_map, \
    generate_hierarchy_map, generate_kmeans_map, plot_cluster_map
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn import datasets


def prep_process(df):
    scaler = StandardScaler()
    return scaler.fit_transform(df)


def plot_all_kmeans_maps(somMap, unitsWithVecs, n_clusters, circle_size_ratio=0.20, unit_size=1.0):
    kmeans_map = generate_kmeans_map(somMap.map_weight_df, somMap.x_dim, somMap.y_dim, n_clusters,
                                     preprocess=prep_process)

    kmeans_data_map = generate_data_kmeans_map(unitsWithVecs.unit_map, unitsWithVecs.input_vec_df,
                                               n_clusters=n_clusters, preprocess=prep_process)
    plot_cluster_map(kmeans_map, unit_map=unitsWithVecs.unit_map, circle_size_ratio=circle_size_ratio,
                     unit_size=unit_size)
    plot_data_cluster_map(kmeans_data_map, somMap.x_dim, somMap.y_dim, circle_size_ratio=circle_size_ratio,
                          unit_size=unit_size)
    plot_data_cluster_map(kmeans_data_map, somMap.x_dim, somMap.y_dim, circle_size_ratio=circle_size_ratio, mode='pie',
                          unit_size=unit_size)


def plot_all_hirachy_maps(somMap, unitsWithVecs, n_clusters, linkage, circle_size_ratio=0.20, unit_size=1.0):
    hirachy_map = generate_hierarchy_map(somMap.map_weight_df, somMap.x_dim, somMap.y_dim, n_clusters,
                                         linkage=linkage, preprocess=prep_process)
    hirachy_data_map = generate_data_hierarchy_map(unitsWithVecs.unit_map, unitsWithVecs.input_vec_df,
                                                   n_clusters=n_clusters, linkage=linkage,
                                                   preprocess=prep_process)
    plot_cluster_map(hirachy_map, unit_map=unitsWithVecs.unit_map, circle_size_ratio=circle_size_ratio,
                     unit_size=unit_size)
    plot_data_cluster_map(hirachy_data_map, somMap.x_dim, somMap.y_dim, circle_size_ratio=circle_size_ratio, mode='pie',
                          unit_size=unit_size)


if __name__ == '__main__':

    """
    ten_cluster_map = SOMMap.from_somtoolbox("../data/10Clusters/10clusters_trained.wgt")
    ten_cluster_units_with_vecs = UnitsMapWithLinkedVectors.from_somtoolbox("../data/10Clusters/10clusters_trained.unit",
                                                                            "../data/10Clusters/10clusters.vec")

    plot_all_kmeans_maps(ten_cluster_map, ten_cluster_units_with_vecs, 10)
    #plot_all_hirachy_maps(ten_cluster_map, ten_cluster_units_with_vecs, 10, 'single')
    #plot_all_hirachy_maps(ten_cluster_map, ten_cluster_units_with_vecs, 10, 'complete')
    #plot_all_hirachy_maps(ten_cluster_map, ten_cluster_units_with_vecs, 10, 'ward')



    chainlink_map = SOMMap.from_somtoolbox("../data/chainlink/chainlink_trained.wgt")
    chainlink_units_with_vecs = UnitsMapWithLinkedVectors.from_somtoolbox("../data/chainlink/chainlink_trained.unit",
                                                                          "../data/chainlink/chainlink.vec")
    plot_all_kmeans_maps(chainlink_map, chainlink_units_with_vecs, 2)
    plot_all_hirachy_maps(chainlink_map, chainlink_units_with_vecs, 2, 'single')
    #plot_all_hirachy_maps(chainlink_map, chainlink_units_with_vecs, 2, 'average')
    #plot_all_hirachy_maps(chainlink_map, chainlink_units_with_vecs, 2, 'complete')
    #plot_all_hirachy_maps(chainlink_map, chainlink_units_with_vecs, 2, 'ward')
    """


    """
    # IRIS with Minisom
    iris = datasets.load_iris().data
    min_max_scaler = MinMaxScaler()
    iris = min_max_scaler.fit_transform(iris)
    s = som.MiniSom(10, 10, iris.shape[1], sigma=0.8, learning_rate=0.7)
    s.train_random(iris, 30000, verbose=True)
    iris_minisom_map = SOMMap.from_minisom(s)
    iris_minisom_kmeans_map = generate_kmeans_map(iris_minisom_map.map_weight_df, iris_minisom_map.x_dim,
                                                  iris_minisom_map.y_dim, 3)
    iris_minsom_units = UnitsMapWithLinkedVectors.from_minisom(s, iris)

    plot_all_kmeans_maps(iris_minisom_map, iris_minsom_units, 3)
    """

    """
    # IRIS from Toolbox
    iris_toolbox_map = SOMMap.from_somtoolbox("../data/iris/iris.wgt")
    iris_toolbox_kmeans_map = generate_kmeans_map(iris_toolbox_map.map_weight_df, iris_toolbox_map.x_dim,
                                                  iris_toolbox_map.y_dim, 3)

    iris_toolbox_units = UnitsMapWithLinkedVectors.from_somtoolbox("../data/iris/iris.unit",
                                                                   "../data/iris/iris.vec")
    plot_all_kmeans_maps(iris_toolbox_map,  iris_toolbox_units, 3)
    """

    """
    # Train Chainlink
    chainlink_data = read_somtoolbox_input_to_df("../data/chainlink/chainlink.vec")
    chainlink_data = prep_process(chainlink_data.to_numpy())
    mini_som = som.MiniSom(18, 12, chainlink_data.shape[1])
    mini_som.train_random(chainlink_data, 50000, verbose=True)
    chainlink_minisom = SOMMap.from_minisom(mini_som)
    chainlink_units = UnitsMapWithLinkedVectors.from_minisom(mini_som, chainlink_data)
    plot_all_hirachy_maps(chainlink_minisom, chainlink_units, 2, 'single')
    """

    # Train 10 Cluster
    tencluster_data = read_somtoolbox_input_to_df("../data/10Clusters/10clusters.vec")
    tencluster_data = prep_process(tencluster_data.to_numpy())
    mini_som = som.MiniSom(10, 10, tencluster_data.shape[1])
    mini_som.train_random(tencluster_data, 1000, verbose=True)
    tencluster_minisom = SOMMap.from_minisom(mini_som)
    tencluster_units = UnitsMapWithLinkedVectors.from_minisom(mini_som, tencluster_data)
    plot_all_kmeans_maps(tencluster_minisom, tencluster_units, 10, unit_size=0.5)
