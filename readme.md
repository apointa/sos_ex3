# SOM - Visualization - Clustering

This implementation was done as an assignment in the course Self Organizing System at TU Vienna.
It provides a SOM clustering visualization. The framework supports SOMs trained by
[miniSOM](https://github.com/laitom/minSOM) and [SomToolbox](http://www.ifs.tuwien.ac.at/dm/somtoolbox) .

To get an overview of the functionality check out the [usage_guide jupiter notebook](notebooks/usage_guide.ipynb).
The [evaluation report notebook](notebooks/evaluation_report.ipynb) was part of the assignment and show a further analysis.
The [datasets](http://www.ifs.tuwien.ac.at/dm/somtoolbox/datasets.html)) for the evaluation where provided by the lecture.
